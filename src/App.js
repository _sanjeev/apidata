import React, { Component } from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Post from "./Post";
import Users from "./Users";
import Comments from "./Comments";

class App extends Component {
  // constructor(props) {
  //   super(props);
  //   // this.state = {
  //   //   users: [],
  //   //   posts: [],
  //   //   comments: [],
  //   //   error: ''
  //   // }
  // }

  // componentDidUpdate = () => {

  // }

  // handlePost = async (event) => {
  //   try {
  //     const post = await fetch('https://jsonplaceholder.typicode.com/users/' + event.target.id + '/posts');
  //     const postData = await post.json();
  //     console.log(postData);
  //     this.setState({ posts: postData });
  //   } catch (error) {
  //     this.state({
  //       error: 'Not found the data',
  //     })
  //   }
  // }

  // fetchData = async () => {
  //   const userResponse = await fetch('https://jsonplaceholder.typicode.com/users');
  //   const userData = await userResponse.json();

  //   // const postResponse = await fetch('https://jsonplaceholder.typicode.com/posts');
  //   // const postData = await postResponse.json();

  //   // const commentResponse = await fetch('https://jsonplaceholder.typicode.com/comments');
  //   // const commentData = await commentResponse.json();
  //   this.setState({
  //     users: userData,
  //   });
  // }

  // componentDidMount = () => {
  //   this.fetchData();
  // }

  render() {
    // const { users } = this.state;
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Users} />
          <Route exact path="/posts/:id" component={Post} />
          <Route exact path="/comments/:id" component={Comments} />
        </Switch>
      </Router>
    );
  }
}

export default App;
