import React, { Component } from "react";
import { BrowserRouter as Router, Link, Switch, Route } from "react-router-dom";
import "./user.css";

class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      randomImage: [],
      error: "",
    };
  }

  fetchData = async () => {
    try {
      const userResponse = await fetch(
        "https://jsonplaceholder.typicode.com/users"
      );
      const imgResponse = await fetch("https://randomuser.me/api/?results=10");
      const imgData = await imgResponse.json();
      const userData = await userResponse.json();
      this.setState({
        users: userData,
        randomImage: imgData.results,
      });
    } catch (error) {
      this.setState({
        error: "Not found the data",
      });
    }
  };

  componentDidMount = () => {
    this.fetchData();
  };

  render() {
    const { users, imgData } = this.state;
    return (
      <div>
        <h1 className="heading">Users</h1>
        {users.map((item, index) => (
          <div className="container">
            <Link to={`/posts/${item.id}`} className="link" key={item.id}>
              <div className="header">
                <img
                  src={this.state.randomImage[index].picture.medium}
                  alt="Profile"
                />
                <p> {item.username}</p>
              </div>
              <img
                src={this.state.randomImage[index].picture.medium}
                alt="Profile"
              />
              {/* <p className="para">id: {item.id}</p> */}
              <p className="para">{item.name}</p>
              <p className="para">Email: {item.email}</p>
              <p className="para">
                Address: {item.address.street} {item.address.suite},{" "}
                {item.address.city}, {item.address.zipcode}
              </p>
              <p className="para">Phone:- {item.phone}</p>
              <p className="para"> Website:- {item.website}</p>
              <p className="para">
                Company:- {item.company.name} {item.company.bs}
              </p>
              <p className="para">Company Phrase:- {item.company.catchPhrase}</p>
              <p className="para">Company Business:- {item.company.bs}</p>
            </Link>
          </div>
        ))}
      </div>
    );
  }
}

export default Users;
