import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import "./post.css";
class Post extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      randomImage: [],
      error: "",
    };
  }
  fetchData = async () => {
    const res = await fetch(
      "https://jsonplaceholder.typicode.com/users/" +
        this.props.match.params.id +
        "/posts"
    );
    const imgResponse = await fetch("https://randomuser.me/api/?results=10");
    const imgData = await imgResponse.json();
    const postData = await res.json();
    this.setState({ posts: postData, randomImage: imgData.results });
  };

  componentDidMount() {
    this.fetchData();
  }

  render() {
    const { posts } = this.state;

    return (
      <div>
        <h1 className="heading">Posts</h1>
        <div className="post-container">
        {posts.map((item, index) => (
          <div className="sec-container">
            <Link to={{ pathname: `/comments/${item.id}` }} className="link">
              <img
                src={this.state.randomImage[index].picture.medium}
                alt="Profile"
              />
              {/* <p>id: {item.id}</p> */}
              {/* <p>{item.userId}</p> */}
              <p>{item.title}</p>
              <p>{item.body}</p>
            </Link>
          </div>
        ))}
        </div>
      </div>
    );
  }
}

export default withRouter(Post);
