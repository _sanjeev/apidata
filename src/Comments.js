import React, { Component } from "react";
import './comments.css';

class Comments extends Component {
  constructor(props) {
    super(props);
    console.log(this.props.match.params.id);
    this.state = {
      comments: [],
      randomImage: [],
      error: ''
    };
  }

  fetchData = async () => {
    const res = await fetch(
      "https://jsonplaceholder.typicode.com/posts/" +
        this.props.match.params.id +
        "/comments"
    );
    const userComments = await res.json();
    const imgResponse = await fetch("https://randomuser.me/api/?results=10");
    const imgData = await imgResponse.json();
    this.setState({ comments: userComments, randomImage: imgData.results });
  };

  componentDidMount = () => {
    this.fetchData();
  };

  render() {
    const { comments } = this.state;
    return (
      <div>
      <h1 className="heading">Comments</h1>
        {comments.map((item, index) => (
          <div key={item.id} className="comments">
          <div className="header">
              <img src={this.state.randomImage[index].picture.medium} alt="Profile Image" />
              
              <div>
                  <p>{item.name}</p>
                  <p>{item.email}</p>
              </div>
          </div>
            {/* <p>postId: {item.postId}</p>
            <p>id: {item.id}</p> */}
            {/* <p>Name: {item.name}</p>
            <p>Email: {item.email}</p> */}
            <p className="para"> {item.body}</p>
          </div>
        ))}
      </div>
    );
  }
}

export default Comments;
